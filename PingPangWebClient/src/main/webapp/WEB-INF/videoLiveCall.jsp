<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>直播</title>
    <link rel="stylesheet" href="${httpServletRequest.getContextPath()}/audio/video-js.min.css">
    <script src="${httpServletRequest.getContextPath()}/audio/peer.min.js"></script>
    <script src="${httpServletRequest.getContextPath()}/jquery.min.js"></script>
    <script src="${httpServletRequest.getContextPath()}/audio/video.min.js"></script>
</head>

<body>
    <table>
        <tr style = "display : none">
            <td align="right">
                <label for="txtSelfId">当前用户:</label>
                <input type="text" id="txtSelfId" class="w120" readonly="readonly"/>
                <label for="txtTargetId">目标用户:</label>
                <input type="text" id="txtTargetId" class="w120" readonly="readonly"/>
            </td>
        </tr>
        <tr>
            <td>
             <div style="width: 300px; height: 300px">
                <video id="remoteVideo2" class="video-js"  preload="auto" controls  width="300" height="300" poster="${httpServletRequest.getContextPath()}/audio/timg.jfif">
                </video>
              </div> 
            </td>
            <td>
            <div id="viewMsg"></div>
            </td>
        </tr>
        <tr>
            <td align="center"><span id="remoteUser"></span></td>
        </tr>
    </table>
    <canvas width="400" id="canvas" height="300" style="display: none" ></canvas>
    <%-- <script src="${httpServletRequest.getContextPath()}/audio/videoLiveCall.js"></script> --%>
    <script type="text/javascript">
    //设置中文
	  videojs.addLanguage('zh-CN', {
		  "Play": "播放",
		  "Pause": "暂停",
		  "Current Time": "当前时间",
		  "Duration": "时长",
		  "Remaining Time": "剩余时间",
		  "Stream Type": "媒体流类型",
		  "LIVE": "直播中",
		  "Loaded": "加载完毕",
		  "Progress": "进度",
		  "Fullscreen": "全屏",
		  "Non-Fullscreen": "退出全屏",
		  "Mute": "静音",
		  "Unmute": "取消静音",
		  "Playback Rate": "播放速度",
		  "Subtitles": "字幕",
		  "subtitles off": "关闭字幕",
		  "Captions": "内嵌字幕",
		  "captions off": "关闭内嵌字幕",
		  "Chapters": "节目段落",
		  "Close Modal Dialog": "关闭弹窗",
		  "Descriptions": "描述",
		  "descriptions off": "关闭描述",
		  "Audio Track": "音轨",
		  "You aborted the media playback": "视频播放被终止",
		  "A network error caused the media download to fail part-way.": "网络错误导致视频下载中途失败。",
		  "The media could not be loaded, either because the server or network failed or because the format is not supported.": "视频因格式不支持或者服务器或网络的问题无法加载。",
		  "The media playback was aborted due to a corruption problem or because the media used features your browser did not support.": "由于视频文件损坏或是该视频使用了你的浏览器不支持的功能，播放终止。",
		  "No compatible source was found for this media.": "无法找到此视频兼容的源。",
		  "The media is encrypted and we do not have the keys to decrypt it.": "视频已加密，无法解密。",
		  "Play Video": "播放视频",
		  "Close": "关闭",
		  "Modal Window": "弹窗",
		  "This is a modal window": "这是一个弹窗",
		  "This modal can be closed by pressing the Escape key or activating the close button.": "可以按ESC按键或启用关闭按钮来关闭此弹窗。",
		  ", opens captions settings dialog": ", 开启标题设置弹窗",
		  ", opens subtitles settings dialog": ", 开启字幕设置弹窗",
		  ", opens descriptions settings dialog": ", 开启描述设置弹窗",
		  ", selected": ", 选择",
		  "captions settings": "字幕设定",
		  "Audio Player": "音频播放器",
		  "Video Player": "视频播放器",
		  "Replay": "重播",
		  "Progress Bar": "进度小节",
		  "Volume Level": "音量",
		  "subtitles settings": "字幕设定",
		  "descriptions settings": "描述设定",
		  "Text": "文字",
		  "White": "白",
		  "Black": "黑",
		  "Red": "红",
		  "Green": "绿",
		  "Blue": "蓝",
		  "Yellow": "黄",
		  "Magenta": "紫红",
		  "Cyan": "青",
		  "Background": "背景",
		  "Window": "视窗",
		  "Transparent": "透明",
		  "Semi-Transparent": "半透明",
		  "Opaque": "不透明",
		  "Font Size": "字体尺寸",
		  "Text Edge Style": "字体边缘样式",
		  "None": "无",
		  "Raised": "浮雕",
		  "Depressed": "压低",
		  "Uniform": "均匀",
		  "Dropshadow": "下阴影",
		  "Font Family": "字体库",
		  "Proportional Sans-Serif": "比例无细体",
		  "Monospace Sans-Serif": "单间隔无细体",
		  "Proportional Serif": "比例细体",
		  "Monospace Serif": "单间隔细体",
		  "Casual": "舒适",
		  "Script": "手写体",
		  "Small Caps": "小型大写字体",
		  "Reset": "重启",
		  "restore all settings to the default values": "恢复全部设定至预设值",
		  "Done": "完成",
		  "Caption Settings Dialog": "字幕设定视窗",
		  "Beginning of dialog window. Escape will cancel and close the window.": "开始对话视窗。离开会取消及关闭视窗",
		  "End of dialog window.": "结束对话视窗"
		});
	
	//对方图像
	 let new_source = [];
	 var videoStart=0;
	 var canvas = $("#canvas"); 
	 var ctx = canvas.get()[0].getContext('2d');
	 //var remoteVideoPlayer = videojs('remoteVideo',{ autoplay: true,preload:"auto",controlBar: false });
	 //controlBar:{fullscreenToggle:false}要禁用全屏控件
	 var remoteVideoPlayer2 = videojs('remoteVideo2',{ autoplay: true,preload:"auto",controls: false,fluid: true,sources:new_source});
	
	    remoteVideoPlayer2.on('ended',function(e) {
			 console.log("ended2:"+"_"+index);
			 ctx.drawImage(remoteVideoVid2, 0, 0, 400, 300);
	         let src=canvas.get()[0].toDataURL("image/jpeg");
	         remoteVideoPlayer2.poster(src);
	         remoteVideoPlayer2.src(new_source.slice(index-1));
		 });
	 
	 var remoteVideoVid2= remoteVideoPlayer2.tech({ IWillNotUseThisInPlugins: true }).el();
		    
	 //接收数据
	 var index=0;
	 var successStream;
	 var oSourceBuffer, oMediaSource;
	 var mimeCodec='video/webm; codecs="vp8,opus"';
	     mimeCodec='video/webm;codecs=h264';
			      //{src:"//www.domain.com/video_720p.mp4",type:"video/mp4",res:"720",label:"720p"}, 
			      //{src:"//www.domain.com/video_480px.mp4",type:"video/mp4",res:"480",label:"480p"}, 
			      //{src:"//www.domain.com/videos_360p.mp4",type:"video/mp4",res:"360",label:"360p",default:true},
			      //{src:"//www.domain.com/videos_240p.mp4",type:"video/mp4",res:"240",label:"240p"}
	 function remotStream(stream){
		 console.log(index);
         new_source.push({type:stream.split(';base64')[0].split(':')[1], src: window.URL.createObjectURL(dataURItoBlob(stream))});
         if(0==index){
        	 remoteVideoPlayer2.src(new_source[0]);
   		     remoteVideoPlayer2.load();
             remoteVideoPlayer2.play();
         }
         index=index+1;
	 }
	 
	    /**
	     * base64  to blob二进制
	     *data:video/webm;codecs=vp8,opus;base64,
	     *data:video/x-matroska;codecs=avc1,opus;base64,
	     *dataURI.split('base64')[0].split(':')[1].split(';')[0]
	     */
	    function dataURItoBlob(dataURI) {
	        var mimeString = dataURI.split(';base64')[0].split(':')[1]; // mime类型
	        console.log(mimeString);//video/webm
	        //console.log(dataURI.split('base64,')[1]);
	        var byteString = atob(dataURI.split('base64,')[1]); //base64 解码
	        var arrayBuffer = new ArrayBuffer(byteString.length); //创建缓冲数组
	        var intArray = new Uint8Array(arrayBuffer); //创建视图

	        for (var i = 0; i < byteString.length; i++) {
	            intArray[i] = byteString.charCodeAt(i);
	        }
	        //return new Blob([intArray]);
	        return new Blob([intArray], {type: mimeString});
	    }
	     
	     function dataURItoArray(dataURI,headStart) {
		        var mimeString = dataURI.split(';base64')[0].split(':')[1]; // mime类型
		        console.log(headStart+"_"+mimeString);//video/webm
		        //console.log(dataURI.split('base64,')[1]);
		        var byteString = atob(dataURI.split('base64,')[1]); //base64 解码
		        var arrayBuffer = new ArrayBuffer(byteString.length); //创建缓冲数组
		        var intArray = new Uint8Array(arrayBuffer); //创建视图

		        for (var i = 0; i < byteString.length; i++) {
		            intArray[i] = byteString.charCodeAt(i);
		        }
		        //return new Blob([intArray]);
		        return  intArray.subarray(headStart);
	   }	  
</script>
</body>

</html>