<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8">
  <title></title> 
  <link rel="stylesheet" href="${httpServletRequest.getContextPath()}/x-admin/css/font.css">
  <link rel="stylesheet" href="${httpServletRequest.getContextPath()}/x-admin/css/login.css">
  <link rel="stylesheet" href="${httpServletRequest.getContextPath()}/x-admin/css/xadmin.css">
  
  <script src="${httpServletRequest.getContextPath()}/jquery.min.js"></script>
  <script src="${httpServletRequest.getContextPath()}/layer.js"></script>
  <script src="${httpServletRequest.getContextPath()}/layui.js"></script> 
</head>
<body class="login-bg">

<div class="login layui-anim layui-anim-up">
        <div class="message">PingPangChat</div>
        <div id="darkbannerwrap"></div>
   <form class="layui-form" action="${httpServletRequest.getContextPath()}/user/chat" method="POST">
   
    <input name="userCode" id="userCode" placeholder="编码"  type="text" autocomplete="off"  lay-verify="required|userName" class="layui-input">
            <hr class="hr15">
    <input name="userPassword" id="userPassword" placeholder="密码"  type="text" autocomplete="off" lay-verify="required|userCode" class="layui-input">
            <hr class="hr15">
            
    <button class="layui-btn" lay-filter="login"  lay-submit="" style="width:100%;">提交</button>
    <hr class="hr15">
  </form>
    <button class="layui-btn"  style="width:100%;" onclick="location.href='${httpServletRequest.getContextPath()}/userController/regist'" >注册</button>
</div> 
  <script>
             layui.use(['form','layer','laydate' ], function() {
					var form = layui.form
					   ,layer = layui.layer
					   ,layedit = layui.layedit
					   ,laydate = layui.laydate;
					 
					//自定义验证规则
					form.verify({
						userName : function(value) {
							if (value.length < 2) {
								return '代号至少需要两个字符';
							}
						},
						userCode : [ /^[0-9A-Za-z]+$/, '字母和数字' ]
					});

					//监听提交
					form.on('submit(login)', function(data) {
						/*layer.alert(JSON.stringify(data.field), {
						  title: '最终的提交信息'
						})*/
						return true;
					});

					if (null != "${userCode}" && "" != "${userCode}") {
						 $("#userCode").val("${userCode}");
					}
					
					if (null != "${userPassword}" && "" != "${userPassword}") {
						 $("#userPassword").val("${userPassword}");
					}
					
					var errorMsg = "${errorMsg}";
					if (null != errorMsg && "" != errorMsg) {
						layer.alert(errorMsg), {
							title : '错误提示'
						}
					}
					
					
				});
		</script>
</body>
</html>