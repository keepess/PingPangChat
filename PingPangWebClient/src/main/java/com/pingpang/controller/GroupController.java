package com.pingpang.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pingpang.service.UserGroupService;
import com.pingpang.util.PageUtil;
import com.pingpang.util.StringUtil;
import com.pingpang.websocketchat.ChartUser;
import com.pingpang.websocketchat.ChatGroup;

@RestController
@RequestMapping("/group")
public class GroupController {

	@Autowired
	private UserGroupService userGroupService;
	
	/**
	  * 列表页面
	 * @return
	 */
	@RequestMapping("/db-group-list")
	public ModelAndView dbGroupList() {
		return new ModelAndView("db-group-list");
	}
	
	/**
	  * 列表页面
	 * @return
	 */
	@RequestMapping("/add-group-index")
	public ModelAndView addGroupIndex() {
		return new ModelAndView("add-group");
	}
	
	@RequestMapping("/chat-add-group")
	public ModelAndView addChatGroupIndex() {
		return new ModelAndView("chat-add-group");
	}
	
	
	@ResponseBody
	@RequestMapping(value="/db-group-add")
	public Map<String,Object> addGroup(HttpServletRequest request,ChatGroup chatGroup){
		if(null==chatGroup || StringUtil.isNUll(chatGroup.getGroupCode())) {
			return StringUtil.returnMap("F","账号为空!");
		}
		
	    ChartUser cu=(ChartUser) request.getSession().getAttribute("user");
	    chatGroup.setGroupUserCode(cu.getUserCode());
	    chatGroup.setGroupUserID(cu.getId());
	    chatGroup.setGroupStatus("0");
	    return userGroupService.addGroup(chatGroup);
	}
	
	/**
	 * 获取用户群组信息
	 */
	@ResponseBody
	@RequestMapping(value="/db-list")
	public Map<String,Object> dbMsgListData(@RequestBody Map<String,String> queryMap) throws JsonParseException, JsonMappingException, IOException{
		queryMap.putAll(PageUtil.getPage(queryMap.get("page"), queryMap.get("limit")));
		ObjectMapper mapper = new ObjectMapper(); 
		if(!StringUtil.isNUll(queryMap.get("search"))) {
			queryMap.putAll(mapper.readValue(queryMap.get("search"), Map.class));
			queryMap.remove("search");
		}
		
		queryMap.putAll(PageUtil.getPage(queryMap.get("page"), queryMap.get("limit")));
		
		if(!StringUtil.isNUll(queryMap.get("search"))) {
			queryMap.putAll(mapper.readValue(queryMap.get("search"), Map.class));
			queryMap.remove("search");
		}
		
		Map<String,Object>resultMap=new HashMap<String,Object>();
		resultMap.put("code", "0");
		resultMap.put("msg", "");
		resultMap.put("page", queryMap.get("page"));
		resultMap.put("limit", queryMap.get("limit"));
		resultMap.put("count", this.userGroupService.getAllGroupCount(queryMap));
		resultMap.put("data",this.userGroupService.getAllGroupSet(queryMap));
		return resultMap;
	}
	
	
	/**
	   * 下线
	 * @param code
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/db-manager", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> dbDownMsgAll(@RequestParam("ids") Set<String> ids,@RequestParam("status")String status) throws Exception {
		this.userGroupService.updateGroupS(status,ids);
		return StringUtil.returnSucess();
	}
}
