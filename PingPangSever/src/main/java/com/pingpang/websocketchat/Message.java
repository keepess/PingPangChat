package com.pingpang.websocketchat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.pingpang.util.StringUtil;

public class Message implements Serializable {

	private static final long serialVersionUID = 4428353111235809164L;

	private String cmd;// 1.绑定上线 2.下线 3.单聊 4.群聊 5.获取用户信息 6获取群组用户信息
	private String cmdChild;//子命令
	private ChartUser from;// 用户ID
	private ChartUser accept;// 接受ID
    private ChatGroup group;//群组信息
	private String msg;// 文本信息
	private String status;// 信息状态0未发送 1已发送 -1删除
	private String id;// 信息ID
	private String createDate;// 创建时间
	private Set<ChartUser> chatSet;//查询在线用户数的
    private Set<ChatGroup> groupSet;//查询群组数
	private List<Message> oldMsg;//历史信息查询
	
	public String getCmd() {
		return cmd;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	public ChartUser getFrom() {
		return from;
	}

	public void setFrom(ChartUser from) {
		this.from = from;
	}

	public ChartUser getAccept() {
		return accept;
	}

	public void setAccept(ChartUser accept) {
		this.accept = accept;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Set<ChartUser> getChatSet() {
		return chatSet;
	}

	public void setChatSet(Set<ChartUser> chatSet) {
		this.chatSet = chatSet;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCreateDate() {
		if (StringUtil.isNUll(createDate)) {
			return StringUtil.format(new Date());
		}

		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public ChatGroup getGroup() {
		return group;
	}

	public void setGroup(ChatGroup group) {
		this.group = group;
	}

	public Set<ChatGroup> getGroupSet() {
		return groupSet;
	}
     
	
	public void setGroupSet(Set<ChatGroup> groupSet) {
		this.groupSet = groupSet;
	}

	public List<Message> getOldMsg() {
		return oldMsg;
	}

	public void setOldMsg(List<Message> oldMsg) {
		this.oldMsg = oldMsg;
	}

	public String getCmdChild() {
		return cmdChild;
	}

	public void setCmdChild(String cmdChild) {
		this.cmdChild = cmdChild;
	}

	@Override
	public String toString() {
		return "Message [cmd=" + cmd + ", cmdChild=" + cmdChild + ", from=" + from + ", accept=" + accept + ", group="
				+ group + ", msg=" + msg + ", status=" + status + ", id=" + id + ", createDate=" + createDate
				+ ", chatSet=" + chatSet + ", groupSet=" + groupSet + ", oldMsg=" + oldMsg + "]";
	}
}
