package com.pingpang.websocketchat.send.impl;

import com.pingpang.util.StringUtil;
import com.pingpang.websocketchat.ChannelManager;
import com.pingpang.websocketchat.Message;
import com.pingpang.websocketchat.send.ChatSend;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

public class ChatSendVoice extends ChatSend{

	@Override
	public void send(Message message, ChannelHandlerContext ctx) throws Exception {
		//logger.info("{}->{}语音信息{}",message.getFrom().getUserCode(),message.getAccept().getUserCode(),message.getMsg());
		if(null==message.getAccept()||StringUtil.isNUll(message.getAccept().getUserCode())) {
			logger.info("{}发送信息接收信息方当前没找到",message.getFrom().getUserCode());
			return;
		}

		logger.info("{}->{}语音信息",message.getFrom().getUserCode(),message.getAccept().getUserCode());
		
		message.getFrom().setUserPassword("");
		message.getAccept().setUserPassword("");
		
		message.setFrom(userService.getUser(message.getFrom()));
		message.setAccept(userService.getUser(message.getAccept()));
		
		ChannelManager.getChannel(message.getAccept().getUserCode())
		.writeAndFlush(new TextWebSocketFrame(this.getMapper().writeValueAsString(message)));
		
		//临时性判断下
		if(message.getMsg().length()>10) {
			return;
		}
		
		if("close".equals(message.getMsg())) {
			message.setCmd("3");
			message.setMsg("【语音聊天结束】");
			userMsgService.addMsg(message);
		}
		
		if("refuse".equals(message.getMsg())) {
			message.setCmd("3");
			message.setMsg("【语音聊天被拒绝】");
			userMsgService.addMsg(message);
		}
		
		if("3".equals(message.getCmd())) {
		   ChannelManager.getChannel(message.getAccept().getUserCode())
	 	   .writeAndFlush(new TextWebSocketFrame(this.getMapper().writeValueAsString(message)));
		}
	}
}
