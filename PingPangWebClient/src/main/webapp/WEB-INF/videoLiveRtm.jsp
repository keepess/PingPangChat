<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>视频通话测试</title>
 <style>
        .mainContainer {
            display: block;
            width: 400px;
            margin-left: auto;
            margin-right: auto;
        }

        .urlInput {
            display: block;
            width: 100%;
            margin-left: auto;
            margin-right: auto;
            margin-top: 8px;
            margin-bottom: 8px;
        }

        .centeredVideo {
            display: block;
            width: 100%;
            height: 400px;
            margin-left: auto;
            margin-right: auto;
            margin-bottom: auto;
        }

        .controls {
            display: block;
            width: 100%;
            text-align: left;
            margin-left: auto;
            margin-right: auto;
        }
    </style>
</head>
<body onbeforeunload="flv_destroy();">
    <div class="mainContainer">
        <video muted id="videoElement" class="centeredVideo" controls autoplay width="1024" height="400">Your browser is too old which doesn't support HTML5 video.</video>
    </div>
    <br>
    <!-- <div class="controls">
        <button onclick="flv_load()">加载</button>
        <button onclick="flv_start()">开始</button>
        <button onclick="flv_pause()">暂停</button>
        <button onclick="flv_destroy()">停止</button>
        <input style="width:100px" type="text" name="seekpoint" />
        <button onclick="flv_seekto()">跳转</button>
    </div> -->
    <script src="${httpServletRequest.getContextPath()}/audio/flv.min.js"></script>
    <script>
        var player = document.getElementById('videoElement');
        if (flvjs.isSupported()) {
            console.log('直播间：${liveCode}');
            var flvPlayer = flvjs.createPlayer({
                type: 'flv',
                //"isLive": true,//<====加个这个 
                url:'http://127.0.0.1:8000/live?app=myapp&stream=${liveCode}',//<==自行修改
                //hasAudio: true,
                //hasVideo: true,
                isLive: true,
                withCredentials: false,
                cors: true       
              }, {
            	  enableWorker: true,
            	  enableStashBuffer: false,
            	  stashInitialSize: 120,
                  fixAudioTimestampGap: false  //主要是这个配置，直播时音频的码率会被降低，直播游戏时背景音会有回响，但是说话声音没问题
              });
            flvPlayer.attachMediaElement(videoElement);
            flvPlayer.load(); //加载
            flv_start();
        }

        function flv_start() {
            player.play();
        }

        function flv_pause() {
            player.pause();
        }

        function flv_destroy() {
        	console.log("离开直播间");
            player.pause();
            player.unload();
            player.detachMediaElement();
            player.destroy();
            player = null;
        }

        function flv_seekto() {
            player.currentTime = parseFloat(document.getElementsByName('seekpoint')[0].value);
        }
        
    </script>
</body>

</html>