/**
 * 一对一视频服务
 */

var txtSelfId = document.querySelector("input#txtSelfId");
var txtTargetId = document.querySelector("input#txtTargetId");
//var localVideo = document.querySelector("video#localVideo");
//var remoteVideo = document.querySelector("video#remoteVideo");
var videoSelect = document.querySelector("select#videoSelect");

var mimeType = 'video/webm; codecs="vp8,opus"';
//var mimeType = 'video/mp4; codecs="avc1.42E01E, mp4a.40.2"';//'video/webm;codecs=vp8,opus';
var mediaRecorder;

let peer = null;
let localConn = null;
let localStream = null;

function gotStream(stream) {
    console.log('received local stream');
    localStream = stream;
    //localVideo.srcObject = localStream;
    localVideoVid.srcObject = stream;
    localVideoVid.play();
    
    try {
        var options = {mimeType: mimeType};
        mediaRecorder = new MediaRecorder(stream, options);
    } catch (e) {
        console.error('Exception while creating MediaRecorder: ' + e);
        log('Exception while creating MediaRecorder: ' + e);
        alert('Exception while creating MediaRecorder: '
            + e + '. mimeType: ' + options.mimeType);
        return;
    }
    mediaRecorder.start(1000);
    mediaRecorder.ondataavailable = handleDataAvailable;
}

function handleError(error) {
    console.log('navigator.MediaDevices.getUserMedia error: ', error.message, error.name);
}

//绑定摄像头列表到下拉框
function gotDevices(deviceInfos) {
    if (deviceInfos===undefined){
        return
    }
    for (let i = 0; i !== deviceInfos.length; ++i) {
        const deviceInfo = deviceInfos[i];
        const option = document.createElement('option');
        option.value = deviceInfo.deviceId;
        if (deviceInfo.kind === 'videoinput') {
            option.text = deviceInfo.label || `camera ${videoSelect.length + 1}`;
            videoSelect.appendChild(option);
        }
    }
}

//开启本地摄像头
function start() {
    if (localStream) {
        localStream.getTracks().forEach(track => {
            track.stop();
        });
    }

    const videoSource = videoSelect.value;
    const constraints = {
        audio:  {
                 echoCancellation: true,
                 noiseSuppression: true,
                 autoGainControl: true,
                 sampleRate: 44100,
                 channelCount: 2,
                 volume:1.0
                 },
        video: { width: 320, deviceId: videoSource ? { exact: videoSource } : undefined }
    };

    navigator.mediaDevices
        .getUserMedia(constraints)
        .then(gotStream)
        .then(gotDevices)
        .catch(handleError);
}


function handleDataAvailable(event) {
    if (event.data && event.data.size > 0) {
        console.log('正在发送数据...');
            var reader = new FileReader();
                reader.readAsDataURL(event.data);
                reader.onload = function (e) {
                    //console.info("videoStr:"+reader.result);
                    sendVideo(reader.result);
                    //window.URL.revokeObjectURL(localVideoPlayer.src);
                    }
            //socket.send(event.data);
    }
}

window.onload = function () {
    if (!navigator.mediaDevices ||
        !navigator.mediaDevices.getUserMedia) {
        console.log('webrtc is not supported!');
        alert("webrtc is not supported!");
        return;
    }

    //获取摄像头列表
    navigator.mediaDevices.enumerateDevices()
        .then(gotDevices)
        .catch(handleError);

    videoSelect.onchange = start;

    sleep(500);
    start();
    
    //audioCall();
}
//setTimeout("audioCall()","1000");

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}